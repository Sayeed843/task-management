from django.urls import path
from .views import TaskList, TaskDetail, TaskCreate, TaskUpdate, TaskDelete, CustomLoginView, RegisterPage
from django.contrib.auth.views import LogoutView

app_name = 'todo'

urlpatterns = [
    path('list/', TaskList.as_view(), name='list'),
    path('task/<int:pk>/', TaskDetail.as_view(), name='detail'),
    path('task/create/', TaskCreate.as_view(), name='create'),
    path('task/edit/<int:pk>/', TaskUpdate.as_view(), name='edit'),
    path('task/delete/<int:pk>/', TaskDelete.as_view(), name='delete'),

    path('login/', CustomLoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(next_page='todo:login'), name='logout'),
    path('register/', RegisterPage.as_view(), name='register'),

]

